# Comparing Orderless Elements

*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the XML-Compare release. The resources should be located such that they are two levels below the top level release directory that contains the jar files.*

*For example `DeltaXML-XML-Compare-10_0_0_j/samples/sample-name`.*

---

This sample explains how and why to instruct XML Compare to treat XML  as an orderless set of elements.  For a full description of the concepts, see [Comparing Orderless Elements](https://docs.deltaxml.com/xml-compare/latest/samples-and-guides/comparing-orderless-elements).

If the information you are comparing is mainly data rather than part of a document (mainly text including mixed content), then you may find our new [XML Data Compare: Orderless Comparison](https://docs.deltaxml.com/xdc/latest/samples/orderless-comparison) product more suitable and easier to configure than XML Compare

The sample code illustrates two methods of performing orderless element comparison. The first method uses the Pipelined Comparator, and specifies input and output filters with a DXP file. The second method uses the API exposed by the Document Comparator.

If you have Ant installed, use the build script provided to run the sample. The following command will generate output for the two methods in the PipelinedComparatorResult and DocumentComparatorResult directories.
```
ant run
```
If you don't have Ant installed, you can run the sample from a command line by issuing the commands listed below from the sample directory (ensuring that you use the correct directory and class path separators for your operating system).

## Pipelined Comparator (DXP)
To run just the Pipelined Comparator sample and produce the output files `resultAB.xml` and `resultAC.xml` in the `PipelinedComparatorResult` directory, type the following command:
```
ant run-dxp
```
The commands to run only the Pipelined Comparator sample are as follows. Replace x.y.z with the major.minor.patch version number of your release e.g. `deltaxml-10.0.0.jar`
```
java -jar ../../deltaxml-x.y.z.jar compare orderless documentA.xml documentB.xml resultAB.xml
java -jar ../../deltaxml-x.y.z.jar compare orderless documentA.xml documentC.xml resultAC.xml
```
## Document Comparator (API)
To run just the Document Comparator API sample and produce the output files `resultAB.xml` and `resultAC.xml` in the `DocumentComparatorResult` directory, type the following command:
```
ant run-dc
```
The commands to compile and run only the Document Comparator sample code are as follows. Replace x.y.z with the major.minor.patch version number of your release e.g. `deltaxml-10.0.0.jar`
```
mkdir bin
javac -cp bin:../../deltaxml-x.y.z.jar:../../saxon9pe.jar -d bin ./src/java/com/deltaxml/samples/OrderlessComparisonSample.java
java -cp bin:../../deltaxml-x.y.z.jar:../../saxon9pe.jar:../../icu4j.jar:../../resolver.jar:./bin/com/deltaxml/samples/ com.deltaxml.samples.OrderlessComparisonSample documentA.xml documentB.xml resultAB.xml
java -cp bin:../../deltaxml-x.y.z.jar:../../saxon9pe.jar:../../icu4j.jar:../../resolver.jar:./bin/com/deltaxml/samples/ com.deltaxml.samples.OrderlessComparisonSample documentA.xml documentC.xml resultAC.xml
```

To clean up the sample directory, run the following Ant command.
```
ant clean
```
