<?xml version="1.0" encoding="utf-8"?>
<!-- Copyright (c) 2005-2010 DeltaXML Ltd. All rights reserved -->
<!-- $Id$ -->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
  xmlns:deltaxml="http://www.deltaxml.com/ns/well-formed-delta-v1">
  
  <xsl:output method="xml" indent="no" />
  
  <xsl:template match="@* | node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="meta[preceding-sibling::meta]" mode="#default">
    
  </xsl:template>
  
  <xsl:template match="meta" mode="wrap">
    <xsl:element name="meta">
      <xsl:attribute name="deltaxml:key" select="@name"/>
      <xsl:apply-templates select="@*, node()"/>
    </xsl:element>
  </xsl:template>
  
  <xsl:template match="meta[not(preceding-sibling::meta)]">
    <xsl:element name="meta-wrapper">
      <xsl:attribute name="deltaxml:ordered" select="'false'"/>
      <xsl:element name="meta">
        <xsl:attribute name="deltaxml:key" select="@name"/>
        <xsl:apply-templates select="@*, node()"/>
      </xsl:element>
      <xsl:apply-templates select="following-sibling::meta[generate-id(preceding-sibling::meta[last()]) = generate-id(current())]" mode="wrap"/>
    </xsl:element>
  </xsl:template>
  
</xsl:stylesheet>
