// Copyright (c) 2024 Deltaman group limited. All rights reserved.

package com.deltaxml.samples;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import net.sf.saxon.s9api.Serializer;

import com.deltaxml.core.ComparatorInstantiationException;
import com.deltaxml.core.FilterClassInstantiationException;
import com.deltaxml.core.FilterConfigurationException;
import com.deltaxml.core.FilterParameterizationException;
import com.deltaxml.core.ParserInstantiationException;
import com.deltaxml.core.PipelinedComparatorError;
import com.deltaxml.cores9api.ComparisonCancelledException;
import com.deltaxml.cores9api.ComparisonException;
import com.deltaxml.cores9api.DocumentComparator;
import com.deltaxml.cores9api.DocumentComparator.ExtensionPoint;
import com.deltaxml.cores9api.DuplicateStepNameException;
import com.deltaxml.cores9api.FilterChain;
import com.deltaxml.cores9api.FilterProcessingException;
import com.deltaxml.cores9api.FilterStepHelper;
import com.deltaxml.cores9api.LicenseException;
import com.deltaxml.cores9api.PipelineLoadingException;
import com.deltaxml.cores9api.PipelineSerializationException;
import com.deltaxml.cores9api.config.ModifiedWhitespaceBehaviour;
import com.deltaxml.cores9api.config.OrderlessPresentationMode;
import com.deltaxml.cores9api.config.LexicalPreservationConfig;
import com.deltaxml.cores9api.config.PresetPreservationMode;


public class OrderlessComparisonSample {
  
  public static void main(String[] args) throws ParserInstantiationException, FilterConfigurationException,
      ComparatorInstantiationException, FilterClassInstantiationException, ComparisonException, FilterProcessingException,
      PipelineLoadingException, PipelineSerializationException, LicenseException, FileNotFoundException,
      PipelinedComparatorError, IOException, ComparisonCancelledException {
    
    // initialise files
    File f1= new File(args[0]);
    File f2= new File(args[1]);
    File result= new File(args[2]);

    DocumentComparator dc= new DocumentComparator();
    
    dc.setOutputProperty(Serializer.Property.INDENT, "yes");
    dc.setOutputProperty(Serializer.Property.METHOD, "xml");

    dc.getResultReadabilityOptions().setElementSplittingEnabled(false);
    dc.getResultReadabilityOptions().setModifiedWhitespaceBehaviour(ModifiedWhitespaceBehaviour.NORMALIZE);
    
    LexicalPreservationConfig lexConfig= new LexicalPreservationConfig(PresetPreservationMode.ROUND_TRIP);
    lexConfig.setPreserveIgnorableWhitespace(false);
    dc.setLexicalPreservationConfig(lexConfig);
    
    dc.getOutputFormatConfiguration().setOrderlessPresentationMode(OrderlessPresentationMode.B_DELETES);
    
    FilterStepHelper fsHelper= dc.newFilterStepHelper();
    FilterChain infilters= fsHelper.newFilterChain();

    try {
      infilters.addStep(fsHelper.newSingleStepFilterChain(new File("meta-section-key.xsl"), "meta-section-key"));
      dc.setExtensionPoint(ExtensionPoint.PRE_FLATTENING, infilters);
    } catch (DuplicateStepNameException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (IllegalArgumentException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (IllegalStateException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    dc.compare(f1, f2, result);
  }
}
